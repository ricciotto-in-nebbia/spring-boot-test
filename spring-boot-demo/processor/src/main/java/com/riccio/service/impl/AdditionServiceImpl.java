package com.riccio.service.impl;

import com.riccio.service.AdditionService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class AdditionServiceImpl implements AdditionService {

	@Value("${data.number}")
	private Integer number;

	@Override
	public int sum(int i, int j) {
		System.out.println(number);
		return number + i + j;
	}
}
