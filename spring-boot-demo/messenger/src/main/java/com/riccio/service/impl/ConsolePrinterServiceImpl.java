package com.riccio.service.impl;

import com.riccio.service.ConsolePrinterService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ConsolePrinterServiceImpl implements ConsolePrinterService {

	@Value("${data.name}")
	private String name;

	@Override
	public void print(String s) {
		System.out.println(name + s);
	}
}
