package com.riccio;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = {"com.riccio"})
@Configuration
public class MessengerApplication {

}
