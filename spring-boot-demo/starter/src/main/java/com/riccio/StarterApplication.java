package com.riccio;

import com.riccio.service.AdditionService;
import com.riccio.service.ConsolePrinterService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

//@ComponentScan(basePackages = {"com.riccio"})
//@ComponentScan(basePackageClasses = {ProcessorApplication.class, MessengerApplication.class})
@Import({ProcessorApplication.class, MessengerApplication.class})
@RequiredArgsConstructor
@SpringBootApplication
public class StarterApplication implements CommandLineRunner {

	private final AdditionService additionService;
	private final ConsolePrinterService consolePrinterService;

	public static void main(String[] args) {
		SpringApplication.run(StarterApplication.class, args);
	}

	@Override
	public void run(String... args) {
		int sum = additionService.sum(3, 10);
		consolePrinterService.print(Integer.toString(sum));
	}
}
